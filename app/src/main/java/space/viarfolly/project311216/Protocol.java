package space.viarfolly.project311216;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;

public class Protocol {
    public enum FileType {PROGRAM, IMAGE}
    public enum Action {SEND_PROGRAM, RECEIVE_PROGRAM, SEND_IMAGE, NONE}

    public static final int BLOCK_LENGTH = 131;
    public static final byte START_PROGRAM_1 = -0x80;
    public static final byte[] START_PROGRAM_2 = { -0x80, 0x02, 0x04, 0x04, 0x61 };
    public static final byte[] START_IMAGE = { 0x02, 0x04, 0x04, 0x61 };
    public static final byte NEXT = -0x7A;
    public static final byte STOP = 0x04;

    public static class ProtocolException extends Exception {
    }

    public static byte[] charToByteConverter(String[] arr) {
        byte[] res = new byte[arr.length];
        for (int i = 0; i < arr.length; i++) {
            res[i] = Integer.decode("0x" + arr[i]).byteValue();
        }
        return res;
    }

    public static boolean testConnection(String host, int port, int timeout) {
        try {
            Socket socket = new Socket();
            socket.connect(new InetSocketAddress(host, port),timeout);
            boolean connected = socket.isConnected();
            socket.close();
            return connected;
        } catch (IOException e) {
            return false;
        }
    }
}
