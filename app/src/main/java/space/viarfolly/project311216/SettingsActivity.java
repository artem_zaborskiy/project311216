package space.viarfolly.project311216;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;

public class SettingsActivity extends AppCompatActivity {

    public static final String SETTINGS_SAVED = "settings saved";
    private EditText inputHost;
    private EditText inputPort;
    private EditText inputTimeout;
    private EditText inputSavePath;
    private SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        preferences = getSharedPreferences(MainActivity.APP_PREFERENCES, Context.MODE_PRIVATE);
        inputHost = (EditText)findViewById(R.id.inputHost);
        inputPort = (EditText)findViewById(R.id.inputPort);
        inputTimeout = (EditText)findViewById(R.id.inputTimeout);
        inputSavePath = (EditText)findViewById(R.id.inputSavePath);
        inputHost.setText(preferences.getString(MainActivity.APP_PREFERENCES_HOST, ""));
        inputPort.setText(Integer.toString(preferences.getInt(MainActivity.APP_PREFERENCES_PORT, 0)));
        inputTimeout.setText(Integer.toString(preferences.getInt(MainActivity.APP_PREFERENCES_TIMEOUT, 0)));
        inputSavePath.setText(preferences.getString(MainActivity.APP_PREFERENCES_SAVE_PATH, ""));
    }

    public void onClickBtnSaveSettings(View view) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(MainActivity.APP_PREFERENCES_HOST, inputHost.getText().toString());
        editor.putInt(MainActivity.APP_PREFERENCES_PORT, Integer.parseInt(inputPort.getText().toString()));
        editor.putInt(MainActivity.APP_PREFERENCES_TIMEOUT, Integer.parseInt(inputTimeout.getText().toString()));
        editor.putString(MainActivity.APP_PREFERENCES_SAVE_PATH, inputSavePath.getText().toString());
        editor.apply();
        Intent answerIntent = new Intent();
        answerIntent.putExtra(SETTINGS_SAVED, true);
        setResult(RESULT_OK, answerIntent);
        finish();
    }
}
