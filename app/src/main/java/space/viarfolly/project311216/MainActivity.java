package space.viarfolly.project311216;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.io.*;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MainActivity extends AppCompatActivity {

    public static final String TAG = "MainActivity";

    public static final String APP_PREFERENCES = "main_settings";
    public static final String APP_PREFERENCES_HOST = "host";
    public static final String APP_PREFERENCES_PORT = "port";
    public static final String APP_PREFERENCES_TIMEOUT = "timeout";
    public static final String APP_PREFERENCES_SAVE_PATH = "save_path";
    private SharedPreferences preferences;

    private static final int PROGRAM_FILE_SELECT_CODE = 0;
    private static final int IMAGE_FILE_SELECT_CODE = 1;
    private static final int SETTINGS_SELECT_CODE = 2;

    private ProgressBar progressBar;
    private TextView labelStatus;
    private Button btnReceiveProgram;
    private Button btnSendProgram;
    private Button btnSendImage;
    private SendTask currentSendTask;
    private ReceiveTask currentReceiveTask;
    private boolean isAction;

    private class FileToSend {
        private FileInputStream in;
        private Protocol.FileType fileType;
        private final long fileSize;//in bytes

        private FileToSend(Uri uri, Protocol.FileType fileType) throws IOException {
            this.fileType = fileType;
            in = (FileInputStream)getContentResolver().openInputStream(uri);
            fileSize = in.getChannel().size();
        }
    }

    private class ProgressInfo {
        private int progress;
        private String msg;

        public ProgressInfo(int progress, String msg) {
            this.progress = progress;
            this.msg = msg;
        }
    }

    private class InitTask extends AsyncTask<String, ProgressInfo, Boolean> {

        String host;
        int port;

        @Override
        protected Boolean doInBackground(String... sockets) {
            String socket = sockets[0];
            String[] hostAndPort = socket.split(":");
            host = hostAndPort[0];
            port = Integer.parseInt(hostAndPort[1]);
            return Protocol.testConnection(host, port, timeout());
        }

        protected void onPostExecute(Boolean connected) {
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString(APP_PREFERENCES_HOST, host);
            editor.putInt(APP_PREFERENCES_PORT, port);
            editor.apply();

            String connectionStatus = connected ? getResources().getString(R.string
                    .app_connection_successful) : getResources().getString(R.string
                    .app_connection_failed);
            labelStatus.setText(connectionStatus);
            findViewById(R.id.btnReceiveProgram).setEnabled(connected);
            findViewById(R.id.btnSendImage).setEnabled(connected);
            findViewById(R.id.btnSendProgram).setEnabled(connected);
        }
    }

    private class SendTask extends AsyncTask<FileToSend, ProgressInfo, Boolean> {

        private Socket socket;

        @Override
        protected Boolean doInBackground(FileToSend... fileToSends) {
            try {
                send(fileToSends[0]);
            } catch (Protocol.ProtocolException e) {
                publishProgress(new ProgressInfo(0, getResources().getString(R.string.app_protocol_exception)));
                return false;
            } catch (IOException e) {
                publishProgress(new ProgressInfo(0,
                        getResources().getString(R.string.app_socket_exception) + "/" +
                                getResources().getString(R.string.app_read_file_exception)));
                return false;
            } finally {
                closeConnection();
            }
            return true;
        }

        @Override
        protected void onProgressUpdate(ProgressInfo... progressInfos) {
            ProgressInfo progressInfo = progressInfos[0];
            progressBar.setMax(100);
            progressBar.setProgress(progressInfo.progress);
            if (progressInfo.msg != null) labelStatus.setText(progressInfo.msg);
        }

        private void closeConnection() {
            try {
                socket.close();
            } catch (IOException e) {
                publishProgress(new ProgressInfo(0, getResources().getString(R.string.app_close_socket_exception)));
            }
        }

        private void send(FileToSend fileToSend) throws Protocol.ProtocolException, IOException {

            publishProgress(new ProgressInfo(0, getResources().getString(R.string.app_connection_maintenance_send)));

            socket = new Socket();
            socket.connect(new InetSocketAddress(host(), port()), timeout());
            DataOutputStream out = new DataOutputStream(socket.getOutputStream());
            DataInputStream in = new DataInputStream(socket.getInputStream());

            if (fileToSend.fileType == Protocol.FileType.PROGRAM) {
                out.write(Protocol.START_PROGRAM_1);
                out.write(Protocol.START_PROGRAM_2);
            } else { // IMAGE
                out.write(Protocol.START_IMAGE);
            }
            if (in.readByte() != Protocol.NEXT) throw new Protocol.ProtocolException();
            publishProgress(new ProgressInfo(0, getResources().getString(R.string.app_file_sending)));

            BufferedReader fileReader = new BufferedReader(new InputStreamReader(fileToSend.in));
            float offset = 0;
            while (true) {
                if (isCancelled()) {
                    publishProgress(new ProgressInfo(0, getResources().getString(R.string.app_cancel)));
                    return;
                }
                String line = fileReader.readLine();
                if (line == null) break;
                offset += Protocol.BLOCK_LENGTH;
                String[] bytes = line.split(" ");
                byte[] block = Protocol.charToByteConverter(bytes);
                out.write(block);
                publishProgress(new ProgressInfo((int)(offset / ( fileToSend.fileSize / 3) *
                        100.0), null));
                if (in.readByte() != Protocol.NEXT) throw new Protocol.ProtocolException();
            }
            out.write(Protocol.STOP);
            publishProgress(new ProgressInfo(100, getResources().getString(R.string.app_file_sent)));
        }

        @Override
        protected void onPostExecute(Boolean result) {
            switchActionOff();
        }
    }

    private class ReceiveTask extends AsyncTask<File, ProgressInfo, Boolean> {

        private Socket socket;

        @Override
        protected Boolean doInBackground(File... files) {
            try {
                receive(files[0]);
            } catch (Protocol.ProtocolException e) {
                publishProgress(new ProgressInfo(0, getResources().getString(R.string.app_protocol_exception)));
                return false;
            } catch (IOException e) {
                publishProgress(new ProgressInfo(0,
                        getResources().getString(R.string.app_socket_exception) + "/" +
                                getResources().getString(R.string.app_write_file_exception)));
                return false;
            } finally {
                closeConnection();
            }
            return true;
        }

        @Override
        protected void onProgressUpdate(ProgressInfo... progressInfos) {
            ProgressInfo progressInfo = progressInfos[0];
            if (progressInfo.progress > -1) {
                progressBar.setIndeterminate(false);
                progressBar.setMax(100);
                progressBar.setProgress(progressInfo.progress);
            } else {
                progressBar.setIndeterminate(true);
            }
            if (progressInfo.msg != null) labelStatus.setText(progressInfo.msg);
        }

        private void closeConnection() {
            try {
                socket.close();
            } catch (IOException e) {
                publishProgress(new ProgressInfo(0, getResources().getString(R.string.app_close_socket_exception)));
            }
        }

        private void receive(File fileToSave) throws Protocol.ProtocolException, IOException {

            publishProgress(new ProgressInfo(0, getResources().getString(R.string.app_connection_maintenance_receive)));

            socket = new Socket();
            socket.connect(new InetSocketAddress(host(), port()), timeout());
            DataOutputStream out = new DataOutputStream(socket.getOutputStream());
            DataInputStream in = new DataInputStream(socket.getInputStream());

            if (in.readByte() != Protocol.START_PROGRAM_1) throw new Protocol.ProtocolException();
            for (byte b : Protocol.START_PROGRAM_2) {
                if (in.readByte() != b) {
                    throw new Protocol.ProtocolException();
                }
            }
            publishProgress(new ProgressInfo(-1, getResources().getString(R.string.app_file_receiving)));
            BufferedWriter fileWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileToSave)));
            while (true) {
                if (isCancelled()) {
                    publishProgress(new ProgressInfo(0, getResources().getString(R.string.app_cancel)));
                    return;
                }
                out.write(Protocol.NEXT);
                StringBuilder block = new StringBuilder();
                for (int i = 0; i < Protocol.BLOCK_LENGTH; i++) {
                    byte b = in.readByte();
                    if (i == 0 && b == Protocol.STOP) {
                        fileWriter.flush();
                        fileWriter.close();
                        publishProgress(new ProgressInfo(100, getResources().getString(R.string.app_file_received)));
                    }
                    String hex = Integer.toHexString(b & 0xFF).toUpperCase();
                    if (hex.length() == 1) hex = "0" + hex;
                    block.append(hex).append(" ");
                }
                block.deleteCharAt(block.length() - 1).append("\n");
                fileWriter.write(block.toString());
            }
        }

        @Override
        protected void onPostExecute(Boolean result) {
            switchActionOff();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        progressBar = (ProgressBar) findViewById(R.id.progressStatus);
        labelStatus = (TextView) findViewById(R.id.labelStatus);
        btnReceiveProgram = (Button) findViewById(R.id.btnReceiveProgram);
        btnSendImage = (Button) findViewById(R.id.btnSendImage);
        btnSendProgram = (Button) findViewById(R.id.btnSendProgram);

        preferences = getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);
        if (!preferences.contains(APP_PREFERENCES_SAVE_PATH)) {
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString(APP_PREFERENCES_SAVE_PATH,
                    new File(getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS),getResources().getString(R.string.app_name)).toString());
            editor.apply();
        }
        if (!preferences.contains(APP_PREFERENCES_TIMEOUT)) {
            SharedPreferences.Editor editor = preferences.edit();
            editor.putInt(APP_PREFERENCES_TIMEOUT, 6000);
            editor.apply();
        }

        initConnection();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case SETTINGS_SELECT_CODE:
                if (resultCode == RESULT_OK) {
                    if(data.getBooleanExtra(SettingsActivity.SETTINGS_SAVED, false)) {
                        initConnection();
                    }
                }
                break;
            case PROGRAM_FILE_SELECT_CODE:
            case IMAGE_FILE_SELECT_CODE:
                if (resultCode == RESULT_OK) {

                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setTitle(R.string.app_confirm_send_title);
                    builder.setMessage(R.string.app_confirm_send_message);
                    builder.setCancelable(true);
                    builder.setPositiveButton(R.string.app_confirm_button_text, new AlertDialogClickListener(requestCode == PROGRAM_FILE_SELECT_CODE ?
                            Protocol.FileType.PROGRAM : Protocol.FileType.IMAGE, data.getData()));
                    AlertDialog dialog = builder.create();
                    dialog.show();
                }
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private class AlertDialogClickListener implements DialogInterface.OnClickListener {

        private Protocol.FileType fileType;
        private Uri uri;

        public AlertDialogClickListener(Protocol.FileType fileType, Uri uri) {
            this.fileType = fileType;
            this.uri = uri;
        }

        @Override
        public void onClick(DialogInterface dialog, int which) {
            dialog.dismiss();
            try {
                switchActionOn(fileType == Protocol.FileType.PROGRAM ? Protocol.Action
                        .SEND_PROGRAM : Protocol.Action.SEND_IMAGE);
                FileToSend fileToSend = new FileToSend(uri, fileType);
                currentSendTask = new SendTask();
                currentSendTask.execute(fileToSend);
            } catch (IOException e) {
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    private void initConnection() {
        String socket = preferences.getString(APP_PREFERENCES_HOST, "192.168.10.1") + ":" +
                preferences.getInt(APP_PREFERENCES_PORT, 8080);
        labelStatus.setText(getResources().getString(R.string.app_connection_attempt));
        btnReceiveProgram.setEnabled(false);
        btnSendImage.setEnabled(false);
        btnSendProgram.setEnabled(false);
        new InitTask().execute(socket);
    }

    private String host() {
        if (!preferences.contains(APP_PREFERENCES_HOST)) throw new RuntimeException();
        return preferences.getString(APP_PREFERENCES_HOST, "");
    }

    private int port() {
        if (!preferences.contains(APP_PREFERENCES_PORT)) throw new RuntimeException();
        return preferences.getInt(APP_PREFERENCES_PORT, 0);
    }

    private int timeout() {
        if (!preferences.contains(APP_PREFERENCES_TIMEOUT)) throw new RuntimeException();
        return preferences.getInt(APP_PREFERENCES_TIMEOUT, 0);
    }

    public void onClickBtnSendProgram(View view) {
        if (isAction) {
            switchActionOff();
            currentSendTask.cancel(true);
        } else {
            showFileChooser(PROGRAM_FILE_SELECT_CODE);
        }
    }

    private void showFileChooser(int fileSelectCode) {
        Intent fileChooser = new Intent(Intent.ACTION_GET_CONTENT);
        fileChooser.setType("*/*");
        fileChooser.addCategory(Intent.CATEGORY_OPENABLE);
        startActivityForResult(Intent.createChooser(fileChooser,
                fileSelectCode == PROGRAM_FILE_SELECT_CODE ? "Выберите файл программы для передачи, каналья!" :
                        "Выберите файл изображения для передачи, тысяча чертей!"), fileSelectCode);
    }

    public void onClickBtnReceiveProgram(View view) {

        if (isAction) {
            switchActionOff();
            currentReceiveTask.cancel(true);
            return;
        }

        String sdState = Environment.getExternalStorageState();
        if (!sdState.equals(Environment.MEDIA_MOUNTED)) {
            Log.d(TAG, "External storage not writable");
            return;
        }

        File dir = new File(preferences.getString(APP_PREFERENCES_SAVE_PATH, ""));

        dir.mkdirs();

        File programFile = new File(
                dir,
                new SimpleDateFormat("yyyy.MM.dd_HH:mm:ss").format(new Date()) + ".txt"
        );

        switchActionOn(Protocol.Action.RECEIVE_PROGRAM);
        currentReceiveTask = new ReceiveTask();
        currentReceiveTask.execute(programFile);
    }

    public void onClickBtnSendImage(View view) {
        if (isAction) {
            switchActionOff();
            currentSendTask.cancel(true);
        } else {
            showFileChooser(IMAGE_FILE_SELECT_CODE);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                Intent settingsActivity = new Intent(this, SettingsActivity.class);
                startActivityForResult(settingsActivity, SETTINGS_SELECT_CODE);
                return true;
            case R.id.action_refresh:
                initConnection();
                return true;
            default:
                return super.onOptionsItemSelected(item);

        }
    }

    private void switchActionOn(Protocol.Action action) {

        btnReceiveProgram.setEnabled(false);
        btnSendImage.setEnabled(false);
        btnSendProgram.setEnabled(false);

        switch (action) {
            case SEND_PROGRAM:
                btnSendProgram.setText(getResources().getString(R.string.app_cancel_send_program));
                btnSendProgram.setEnabled(true);
                break;
            case RECEIVE_PROGRAM:
                btnReceiveProgram.setText(getResources().getString(R.string.app_cancel_receive_program));
                btnReceiveProgram.setEnabled(true);

                break;
            case SEND_IMAGE:
                btnSendImage.setText(getResources().getString(R.string.app_cancel_send_image));
                btnSendImage.setEnabled(true);
                break;
        }

        isAction = true;
    }

    private void switchActionOff() {
        btnReceiveProgram.setEnabled(true);
        btnReceiveProgram.setText(getResources().getString(R.string.app_receive_program));
        btnSendImage.setEnabled(true);
        btnSendImage.setText(getResources().getString(R.string.app_send_image));
        btnSendProgram.setEnabled(true);
        btnSendProgram.setText(getResources().getString(R.string.app_send_program));

        isAction = false;
    }
}
